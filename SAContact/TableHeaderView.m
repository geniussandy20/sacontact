//
//  TableHeaderView.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/5/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "TableHeaderView.h"
#import "UIColor+Utils.h"
#import "UIFont+fonts.h"

#define POINT_SIZE_TO_PIXEL_SIZE(point) ((point)/[UIScreen mainScreen].scale)
#define kAppLeftMargin 16.f
@interface TableHeaderView ()

@property (nonatomic, strong) UIView * topBorder;
@property (nonatomic, strong) UIView * bottomBorder;
@property (nonatomic, strong) UILabel *sectionTitleLabel;
@property (nonatomic, strong) UILabel *sectionCountLabel;

@end

@implementation TableHeaderView

- (instancetype)initWithSectionTitle:(NSString*)sectionTitle
                        sectionCount:(NSString*)sectionCount
                     backgroundColor:(UIColor *)color
                       showTopBorder:(BOOL)showTopBorder
                    showBottomBorder:(BOOL)showBottomBorder
{
    self= [super initWithFrame:CGRectZero];
    if (self)
    {
        self.sectionTitleLabel.text = sectionTitle;
        [self.sectionTitleLabel sizeToFit];
        self.sectionCountLabel.text = sectionCount;
        [self.sectionCountLabel sizeToFit];
        self.backgroundColor = color;
        self.topBorder.hidden = !showTopBorder;
        self.bottomBorder.hidden = !showBottomBorder;
    }
    
    return self;
}

- (instancetype)initWithSectionTitle:(NSString*)sectionTitle
                     backgroundColor:(UIColor *)color
                       showTopBorder:(BOOL)showTopBorder
                    showBottomBorder:(BOOL)showBottomBorder
{
    return  [self initWithSectionTitle:sectionTitle
                          sectionCount:nil
                       backgroundColor:color
                         showTopBorder:showTopBorder
                      showBottomBorder:showBottomBorder];
}

-(UILabel*)sectionTitleLabel
{
    if (!_sectionTitleLabel)
    {
        _sectionTitleLabel = [[UILabel alloc] init];
        _sectionTitleLabel.textAlignment = NSTextAlignmentLeft;
        _sectionTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        _sectionTitleLabel.font = [UIFont fontWithKey:WSFontHelveticaNeue withSize:14];
        _sectionTitleLabel.textColor = [UIColor tableSectionHeaderTitleColor];
        _sectionTitleLabel.frame = CGRectMake(kAppLeftMargin, 0, self.frame.size.width, self.frame.size.height);
        [self addSubview:_sectionTitleLabel];
    }
    return _sectionTitleLabel;
}

-(UILabel*)sectionCountLabel
{
    if (!_sectionCountLabel)
    {
        _sectionCountLabel = [[UILabel alloc] init];
        _sectionCountLabel.textAlignment = NSTextAlignmentLeft;
        _sectionCountLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        _sectionCountLabel.font = [UIFont fontWithKey:WSFontHelveticaNeueBold withSize:14];
        _sectionCountLabel.textColor = [UIColor blackColor];
        _sectionCountLabel.frame = CGRectMake(kAppLeftMargin, 0, self.frame.size.width, self.frame.size.height);
        [self addSubview:_sectionCountLabel];
    }
    return _sectionCountLabel;
}

-(UIView*)border
{
    UIView * border = [[UIView alloc] initWithFrame:CGRectZero];
    border.backgroundColor = [UIColor grayColor];
    border.autoresizingMask= UIViewAutoresizingNone;
    return border;
}

-(UIView *)bottomBorder
{
    if(!_bottomBorder)
    {
        _bottomBorder = [self border];
        [self addSubview:_bottomBorder];
    }
    return _bottomBorder;
}

-(UIView *)topBorder
{
    if(!_topBorder)
    {
        _topBorder = [self border];
        [self addSubview:_topBorder];
    }
    return _topBorder;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGSize size             = self.frame.size;
    CGFloat borderHeight    = POINT_SIZE_TO_PIXEL_SIZE(1.0f);
    self.topBorder.frame    = CGRectMake(0, 0, size.width, borderHeight);
    self.bottomBorder.frame = CGRectMake(0, size.height - borderHeight, size.width, borderHeight);
    self.sectionTitleLabel.frame = CGRectMake(kAppLeftMargin, 0, self.sectionTitleLabel.frame.size.width, self.frame.size.height);
    self.sectionCountLabel.frame = CGRectMake(CGRectGetMaxX(self.sectionTitleLabel.frame) + 2.f, 0, self.sectionCountLabel.frame.size.width, self.frame.size.height);
}


@end
