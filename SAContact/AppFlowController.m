//
//  AppFlowController.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "AppFlowController.h"
#import "ContactListModel.h"
#import "ContactListViewController.h"
#import "AppDelegate.h"

@interface AppFlowController ()

@property (nonatomic,strong) ContactListViewController *contactListViewController;

@end

@implementation AppFlowController

+ (AppFlowController *) sharedInstance
{
    static AppFlowController * appFlowController = nil;
    static dispatch_once_t onceToken;
    if(appFlowController == nil)
    {
        dispatch_once(&onceToken, ^{
            appFlowController = [[AppFlowController alloc] init];
        });
    }
    return appFlowController;
}

-(ContactListViewController *)contactListViewController
{
    if(!_contactListViewController)
    {
        _contactListViewController = [[ContactListViewController alloc] init];
    }
    return _contactListViewController;
}

-(void)setUpApplicationOnLaunch
{
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:self.contactListViewController];
    [ApplicationDelegate.window setRootViewController:navC];
}

@end
