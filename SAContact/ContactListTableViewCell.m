//
//  ContactListTableViewCell.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "ContactListTableViewCell.h"
#import "UIFont+fonts.h"
#import "UIColor+utils.h"
#import "NSString+Utils.h"

#define kContactImageHeight 40.f
#define kContactImageWidth  40.f
#define kAppLeftMargin          16.f    //Left margin across the app
#define kContactProfilePicTopMargin 11.5f
#define kAppHorizontalPadding1  16.f    //Horizontal padding of type1 between to UI across the app
#define kAppHorizontalPadding2  8.f     //Horizontal padding of type2 between to UI across the app
#define kContactDisplayNameTopMargin 10.f


@interface ContactListTableViewCell ()
@property (nonatomic,strong) UIView *contactProfilePicView;
@property (nonatomic,strong) UILabel *contactDisplayName;
@property (nonatomic,strong) UIImageView *contactImageView;
@property (nonatomic,strong) UILabel *contactSecondaryLabel;

@end

@implementation ContactListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)configureWithContact:(ContactModel*)contact
{
    [self configureWithContact:contact searchString:nil];
}

-(void)configureWithContact:(ContactModel*)contact searchString:(NSString*)searchString
{
    self.contactImageView.image = [contact contactImage];
    [self configureContactDisplayNameWithText:[contact contactDisplayName] searchString:searchString];
}

-(UIView *)contactProfilePicView
{
    if (!_contactProfilePicView)
    {
        _contactProfilePicView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, kContactImageWidth, kContactImageHeight)];
        [_contactProfilePicView addSubview:self.contactImageView];
        [self.contentView addSubview:_contactProfilePicView];
    }
    return _contactProfilePicView;
}

-(UIImageView *)contactImageView
{
    if (!_contactImageView)
    {
        _contactImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, kContactImageWidth, kContactImageHeight)];
        _contactImageView.layer.cornerRadius = kContactImageWidth/2;
        _contactImageView.clipsToBounds = YES;
    }
    return _contactImageView;
}

-(UILabel*)contactDisplayName
{
    if(!_contactDisplayName)
    {
        _contactDisplayName = [[UILabel alloc] initWithFrame:CGRectZero];
        _contactDisplayName.textColor = [UIColor cellPrimaryTextLabelColor];
        _contactDisplayName.font = [UIFont fontWithKey:WSFontHelveticaNeueLight withSize:23];
        [self.contentView addSubview:_contactDisplayName];
    }
    return _contactDisplayName;
}

-(UILabel*)contactSecondaryLabel
{
    if(!_contactSecondaryLabel)
    {
        _contactSecondaryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _contactSecondaryLabel.textColor = [UIColor cellPrimaryTextLabelColor];
        _contactSecondaryLabel.font = [UIFont fontWithKey:WSFontHelveticaNeue withSize:14];
        _contactSecondaryLabel.text = @"Search matches other fields";
        [_contactSecondaryLabel sizeToFit];
        [self.contentView addSubview:_contactSecondaryLabel];
    }
    return _contactSecondaryLabel;
}

-(void)configureContactDisplayNameWithText:(NSString*)displayName searchString:(NSString*)searchString
{
    self.contactSecondaryLabel.hidden = YES;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringOrEmptyString:displayName] attributes:@{
                                                                                                                                                            NSFontAttributeName:[UIFont fontWithKey:WSFontHelveticaNeueLight withSize:23],NSForegroundColorAttributeName:[UIColor cellPrimaryTextLabelColor]}];
    
    if (searchString.length)
    {
        NSString *finalSearchString = [NSRegularExpression escapedPatternForString:searchString.lowercaseString];
        NSString *regexString = [NSString stringWithFormat:@"((%@))",finalSearchString];
        NSRange range = [displayName.lowercaseString rangeOfString:regexString options:NSRegularExpressionSearch];
        if (range.location != NSNotFound)
        {
            [attributedString setAttributes: @{
                                               NSFontAttributeName:[UIFont fontWithKey:WSFontHelveticaNeueMedium withSize:23],
                                               NSForegroundColorAttributeName:[UIColor blackColor]
                                               }
                                      range:range];
        }
        else
        {
            self.contactSecondaryLabel.hidden = NO;
        }
    }
    self.contactDisplayName.attributedText = attributedString;
    [self.contactDisplayName sizeToFit];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.contactProfilePicView.frame = CGRectMake(kAppLeftMargin, kContactProfilePicTopMargin, kContactImageWidth, kContactImageHeight);
    
    CGFloat contactDisplayNameLabelWidth = self.frame.size.width - (CGRectGetMaxX(self.contactProfilePicView.frame) + kAppHorizontalPadding1)- (self.accessoryView?(self.accessoryView.frame.size.width+kAppHorizontalPadding2):0);
    
    self.contactDisplayName.frame = CGRectMake((CGFloat)(CGRectGetMaxX(self.contactProfilePicView.frame) + kAppHorizontalPadding1),
                                               kContactDisplayNameTopMargin,
                                               contactDisplayNameLabelWidth,
                                               self.contactDisplayName.frame.size.height);
    self.contactSecondaryLabel.frame = CGRectMake((CGFloat)(CGRectGetMaxX(self.contactProfilePicView.frame) + kAppHorizontalPadding1),
                                                  CGRectGetMaxY(self.contactDisplayName.frame),
                                                  self.contactSecondaryLabel.frame.size.width,
                                                  self.contactSecondaryLabel.frame.size.height);
}

@end
