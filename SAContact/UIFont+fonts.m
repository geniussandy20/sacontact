//
//  UIFont+fonts.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "UIFont+fonts.h"
static NSString * const WSHelveticaNeue         = @"HelveticaNeue";
static NSString * const WSHelveticaNeueLight    = @"HelveticaNeue-Light";
static NSString * const WSHelveticaNeueMedium   = @"HelveticaNeue-Medium";
static NSString * const WSHelveticaNeueBold     = @"HelveticaNeue-Bold";
static NSString * const WSHelveticaNeueThin     = @"HelveticaNeue-Thin";

@implementation UIFont (fonts)

+ (UIFont *)fontWithKey:(WSFont)fontKey withSize:(CGFloat)size {
    NSString *fontString = [UIFont stringForFontKey:fontKey];
    UIFont *desiredFont = nil;
    
    if (fontKey == WSFontSystem) {
        desiredFont = [UIFont systemFontOfSize:size];
    }
    else {
        desiredFont = [UIFont fontWithName:fontString size:size];
    }
    
    return desiredFont;
}

+ (NSString *)stringForFontKey:(WSFont)fontKey {
    NSString *fontString = nil;
    
    switch (fontKey) {
            
        case WSFontHelveticaNeue:
            fontString = WSHelveticaNeue;
            break;
            
        case WSFontHelveticaNeueLight:
            fontString = WSHelveticaNeueLight;
            break;
            
        case WSFontHelveticaNeueMedium:
            fontString = WSHelveticaNeueMedium;
            break;
            
        case WSFontHelveticaNeueBold:
            fontString = WSHelveticaNeueBold;
            break;
            
        case WSFontHelveticaNeueThin:
            fontString = WSHelveticaNeueThin;
            break;
            
        default:
            fontString = nil;
            break;
    }
    
    return fontString;
}

@end
