//
//  NSString+Utils.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

+(NSString *)initialsFromName:(NSString *)name;
+(NSString *)extractNumberFromText:(NSString *)text;
+(NSString*) stringOrEmptyString:(NSString*)string;
@end
