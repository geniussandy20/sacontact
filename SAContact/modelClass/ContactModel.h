//
//  ContactModel
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "JSONModel.h"
#import <UIKit/UIKit.h>

@protocol ContactModel
@end

@interface ContactModel : JSONModel

@property (nonatomic, strong) NSString <Optional>*companyName;
@property (nonatomic, strong) NSArray <Optional>*phones;
@property (nonatomic, strong) NSArray <NSString*><Optional>*addresses;
@property (nonatomic, strong) NSString <Optional>*name;
@property (nonatomic, strong) NSArray <NSString*><Optional>*managers;
@property (nonatomic, strong) NSString <Optional>*parentCompanyName;

//transient property
@property (nonatomic, strong) NSString<Ignore>* contactObjectIdString;

@property (nonatomic, strong) UIImage<Ignore>* contactImage;
@property (nonatomic, strong) NSString<Ignore>* contactDisplayName;
@property (nonatomic, strong) NSString<Ignore> *contactTypeTitleName;

-(BOOL)matchesAnyFieldsWithString:(NSString*)text;
@end
