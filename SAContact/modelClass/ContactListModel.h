//
//  ContactListModel.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "JSONModel.h"
#import "ContactModel.h"

@interface ContactListModel : JSONModel

@property (strong, nonatomic) NSArray<ContactModel> *contacts;

@end
