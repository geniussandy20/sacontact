//
//  ContactModel
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "ContactModel.h"
#import "UIImage+Utils.h"
#import "NSString+Utils.h"

#define kContactImageHeight 40.f
#define kContactImageWidth  40.f

@implementation ContactModel

-(BOOL)validate:(NSError *__autoreleasing *)error
{
    BOOL valid = [super validate:error];
    if (!self.name.length && !self.companyName.length)
    {
        valid = NO;
    }
    return valid;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"parent": @"parentCompanyName",
                                                       }];
}

-(NSString<Ignore> *)contactObjectIdString
{
    if (!_contactObjectIdString)
    {
        _contactObjectIdString = self.name.length?self.name.lowercaseString:self.companyName.lowercaseString;
        NSAssert(_contactObjectIdString.length, @"ContactObjectId string cannot be nil");
    }
    return _contactObjectIdString;
}

-(UIImage<Ignore> *)contactImage
{
    if (!_contactImage)
    {
        if (self.name.length)
        {
            NSString *initials = [NSString initialsFromName:self.name];
            _contactImage = [UIImage imageWithInitials:initials
                                                 width:kContactImageHeight
                                                height:kContactImageWidth];
        }
        else if (self.companyName.length)
        {
            NSString *companyInitials = [NSString initialsFromName:self.companyName];
            _contactImage = [UIImage imageWithInitials:companyInitials
                                                 width:kContactImageHeight
                                                height:kContactImageWidth];
        }
        else
        {
            _contactImage = [UIImage imageNamed:@"ico_profile_default"];
        }
    }
    return _contactImage;
}

-(NSString<Ignore> *)contactDisplayName
{
    if (!_contactDisplayName)
    {
        if (self.name.length)
        {
            _contactDisplayName = [self.name capitalizedString];
        }
        else if(self.companyName.length)
        {
            _contactDisplayName = [self.companyName capitalizedString];
        }
        else
        {
            _contactDisplayName = @"No Name";
        }
    }
    return _contactDisplayName;
}



-(NSString<Ignore> *)contactTypeTitleName
{
    if (!_contactTypeTitleName)
    {
        if (self.name.length)
        {
            _contactTypeTitleName = @"Contact";
        }
        else if(self.companyName.length)
        {
            _contactTypeTitleName = @"Organization";
        }
        else
        {
            _contactTypeTitleName = @"No Name";
        }
    }
    return _contactTypeTitleName;
}

-(BOOL)matchesAnyFieldsWithString:(NSString*)searchText;
{
    BOOL returnValue = NO;
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"self CONTAINS [cd]%@",searchText];
    if ([self.name localizedCaseInsensitiveContainsString:searchText])
    {
        returnValue = YES;
    }
    else if ([self.companyName localizedCaseInsensitiveContainsString:searchText])
    {
        returnValue = YES;
    }
    else if ([self.parentCompanyName localizedCaseInsensitiveContainsString:searchText])
    {
        returnValue = YES;
    }
    else if([self.phones filteredArrayUsingPredicate:bPredicate].count)
    {
        returnValue = YES;
    }
    else if ([self.addresses filteredArrayUsingPredicate:bPredicate].count)
    {
        returnValue = YES;
    }
    else if ([self.managers filteredArrayUsingPredicate:bPredicate].count)
    {
        returnValue = YES;
    }
    return returnValue;
}

@end
