//
//  NSString+Utils.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

+(NSString *)initialsFromName:(NSString *)name
{
    return [NSString stringWithFormat:@"%@",[name substringToIndex:1]];
}

+ (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}

+(NSString*) stringOrEmptyString:(NSString*)string
{
    return string.length == 0 ? @"":string;
}

@end
