//
//  ContactListViewController_Extension.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/6/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "ContactListViewController.h"

@class ContactManager;
@interface ContactListViewController ()
@property (nonatomic,strong) UITableViewController *tableViewController;
@property (nonatomic,strong) NSArray <ContactModel*>*contacts;
@property (nonatomic,strong) NSMutableDictionary <NSString*,ContactModel*> *contactDictionary;

@property (nonatomic,strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic,strong) ContactManager *contactManager;


@property (nonatomic,strong) UIRefreshControl *refreshControl;
@property (nonatomic,strong) UISearchController *searchController;
@property (nonatomic,strong) NSMutableArray *filteredResults;

-(NSMutableDictionary*)parseContactListToDictionary:(NSArray<ContactModel*>*)contacts;

@end
