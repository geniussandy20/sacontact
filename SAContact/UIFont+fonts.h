//
//  UIFont+fonts.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, WSFont) {
    WSFontSystem,
    WSFontHelveticaNeue,
    WSFontHelveticaNeueLight,
    WSFontHelveticaNeueMedium,
    WSFontHelveticaNeueBold,
    WSFontHelveticaNeueThin
};

@interface UIFont (fonts)

+ (UIFont *)fontWithKey:(WSFont)fontKey withSize:(CGFloat)size;


@end
