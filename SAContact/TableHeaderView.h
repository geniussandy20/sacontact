//
//  TableHeaderView.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/5/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableHeaderView : UIView

- (instancetype)initWithSectionTitle:(NSString*)sectionTitle
                        sectionCount:(NSString*)sectionCount
                     backgroundColor:(UIColor *)color
                       showTopBorder:(BOOL)showTopBorder
                    showBottomBorder:(BOOL)showBottomBorder;

- (instancetype)initWithSectionTitle:(NSString*)sectionTitle
                     backgroundColor:(UIColor *)color
                       showTopBorder:(BOOL)showTopBorder
                    showBottomBorder:(BOOL)showBottomBorder;

@end
