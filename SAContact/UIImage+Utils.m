//
//  UIImage+Utils.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "UIImage+Utils.h"
#import "UIFont+fonts.h"
#import "UILabel+Utils.h"
#import "UIColor+utils.h"
#import "UIView+Utils.h"

@implementation UIImage (Utils)

+(UIImage *)imageWithInitials:(NSString *)initials width:(CGFloat)width height:(CGFloat)height
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,width,height)];
    [label setText:initials font:[UIFont fontWithKey:WSFontHelveticaNeue withSize:(height/2.5)] textColor:[UIColor whiteColor] textAlignment:NSTextAlignmentCenter];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.backgroundColor = [UIColor appDisabledActionColor];
    return [label imageForView];
}

@end
