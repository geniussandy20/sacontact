//
//  UIImage+Utils.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

+(UIImage *)imageWithInitials:(NSString *)initials width:(CGFloat)width height:(CGFloat)height;

@end
