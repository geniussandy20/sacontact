//
//  ContactDetailTableViewController.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/5/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "ContactDetailTableViewController.h"
#import "ContactModel.h"
#import "TableHeaderView.h"
#import "UIColor+utils.h"
#import "UIFont+fonts.h"
#import "NSString+Utils.h"

typedef NS_ENUM(NSUInteger , TableViewSections)
{
    kNameSection = 0,
    kPhoneSection,
    kAddressSection,
    kManagersSection,
    kCompanySection,
    NUM_SECTIONS,
};

typedef NS_ENUM(NSUInteger , ContactType)
{
    kContactTypeOrganization = 0,
    kContactTypePerson,
};


static NSString * const nameCellIdentifier = @"nameCellIdentifier";
static NSString * const phoneCellIdentifier = @"phoneCellIdentifier";
static NSString * const addressCellIdentifier = @"addressCellIdentifier";
static NSString * const managersCellIdentifier = @"managersCellIdentifier";
static NSString * const companyCellIdentifier = @"companyCellIdentifier";


@interface ContactDetailTableViewController ()
{
    NSUInteger contactDetailSectionLookUpTable[NUM_SECTIONS];
    NSUInteger contactDetailSectionLookUpTableSize;

}

@property (nonatomic,strong) ContactModel *contact;
@property (nonatomic,assign) ContactType contactType;
@property (nonatomic,weak) id<ContactDetailTableViewDelegate> contactDetailTableViewDelegate;
@end

@implementation ContactDetailTableViewController

-(instancetype)initWithContact:(ContactModel*)contact contactDetailTableViewDelegate:(id<ContactDetailTableViewDelegate>)delegate
{
    self = [super init];
    if (self)
    {
        contactDetailSectionLookUpTableSize = 0;
        self.contactDetailTableViewDelegate = delegate;
        self.contact = contact;
        NSAssert((self.contact.name.length + self.contact.companyName.length) > 0, @"Fatal Error: Incomplete contact information");
        self.contactType = self.contact.name.length?kContactTypePerson:kContactTypeOrganization;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [self.contact contactTypeTitleName];
    self.tableView.backgroundView = [[UIView alloc] init];
    self.tableView.backgroundView.backgroundColor = [UIColor navigationBackgrounColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self rebuilSectionLookUpTable];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return contactDetailSectionLookUpTableSize;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    switch (contactDetailSectionLookUpTable[section])
    {
        case kNameSection:
            numberOfRows = 1;
            break;
        case kPhoneSection:
            numberOfRows = self.contact.phones.count;
            break;
        case kAddressSection:
            numberOfRows = self.contact.addresses.count;
            break;
        case kManagersSection:
            numberOfRows = self.contact.managers.count;
            break;
        case kCompanySection:
            numberOfRows = 1;
            break;
        default:
            break;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (contactDetailSectionLookUpTable[indexPath.section])
    {
        case kNameSection:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:nameCellIdentifier];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nameCellIdentifier];
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
                cell.textLabel.font = [UIFont fontWithKey:WSFontHelveticaNeueMedium withSize:18.0];
                cell.textLabel.textColor = [UIColor cellPrimaryTextLabelColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.textLabel.text = (self.contactType == kContactTypePerson)?self.contact.name:self.contact.companyName;
        }
            break;
        case kPhoneSection:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:phoneCellIdentifier];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:phoneCellIdentifier];
                cell.textLabel.font = [UIFont fontWithKey:WSFontHelveticaNeue withSize:16.0];
                cell.textLabel.textColor = [UIColor cellPrimaryTextLabelColor];
            }
            cell.textLabel.text = self.contact.phones[(NSUInteger) indexPath.row];
        }
            break;
        case kAddressSection:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:addressCellIdentifier];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addressCellIdentifier];
                cell.textLabel.font = [UIFont fontWithKey:WSFontHelveticaNeue withSize:16.0];
                cell.textLabel.textColor = [UIColor cellPrimaryTextLabelColor];
            }
            cell.textLabel.text = self.contact.addresses[(NSUInteger) indexPath.row];
        }
            break;
        case kManagersSection:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:managersCellIdentifier];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:managersCellIdentifier];
                cell.textLabel.font = [UIFont  fontWithKey:WSFontHelveticaNeue withSize:16.0];
                cell.textLabel.textColor = [UIColor cellPrimaryTextLabelColor];
            }
            cell.textLabel.text = self.contact.managers[(NSUInteger) indexPath.row];
        }
            break;
        case kCompanySection:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:companyCellIdentifier];
            if (!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:companyCellIdentifier];
                cell.textLabel.textColor = [UIColor cellPrimaryTextLabelColor];
                cell.textLabel.font = [UIFont  fontWithKey:WSFontHelveticaNeue withSize:16.0];
            }
            cell.textLabel.text = (self.contactType == kContactTypePerson)? self.contact.companyName:self.contact.parentCompanyName;
        }
        default:
            break;
    }
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView;
    switch (contactDetailSectionLookUpTable[section])
    {
        case kNameSection:
            headerView =  nil;
            break;
        case kPhoneSection:
            headerView =  [[TableHeaderView alloc] initWithSectionTitle:[NSLocalizedString(@"Phones",nil) uppercaseString]
                                                           sectionCount:(self.contact.phones.count>1)?[NSString stringWithFormat:@"(%@)",[@(self.contact.phones.count) stringValue]]:@""
                                                        backgroundColor:[UIColor tableViewBackgroundColor]
                                                          showTopBorder:YES
                                                       showBottomBorder:NO];
            
            break;
        case kAddressSection:
            headerView =  [[TableHeaderView alloc] initWithSectionTitle:[NSLocalizedString(@"Addresses",nil) uppercaseString]
                                                           sectionCount:(self.contact.addresses.count>1)?[NSString stringWithFormat:@"(%@)",[@(self.contact.addresses.count) stringValue]]:@""
                                                        backgroundColor:[UIColor tableViewBackgroundColor]
                                                          showTopBorder:YES
                                                       showBottomBorder:NO];
            
            break;
        case kManagersSection:
            headerView =  [[TableHeaderView alloc] initWithSectionTitle:[NSLocalizedString(@"Managers",nil) uppercaseString]
                                                           sectionCount:(self.contact.managers.count>1)?[NSString stringWithFormat:@"(%@)",[@(self.contact.managers.count) stringValue]]:@""
                                                        backgroundColor:[UIColor tableViewBackgroundColor]
                                                          showTopBorder:YES
                                                       showBottomBorder:NO];
            
            break;
        case kCompanySection:
        {
            NSString *title = (self.contactType == kContactTypePerson)? NSLocalizedString(@"Current Organization Name", @"Current Organization Name") : NSLocalizedString(@"Parent Company", @"Parent Company");
            headerView =  [[TableHeaderView alloc] initWithSectionTitle:title
                                                        backgroundColor:[UIColor tableViewBackgroundColor]
                                                          showTopBorder:YES
                                                       showBottomBorder:NO];
        }
            break;
        default:
            break;
    }
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 32.f;
    if (contactDetailSectionLookUpTable[section] == kNameSection)
    {
        headerHeight = 0;
    }
    return headerHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (contactDetailSectionLookUpTable[indexPath.section])
    {
        case kNameSection:
            break;
        case kPhoneSection:
        {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:[NSString extractNumberFromText:self.contact.phones[(NSUInteger)indexPath.row]]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        }
            break;
        case kAddressSection:
            break;
        case kManagersSection:
            [self.contactDetailTableViewDelegate didTapCellWithContactString:[self.contact.managers[(NSUInteger)indexPath.row] lowercaseString]];
            break;
        case kCompanySection:
            [self.contactDetailTableViewDelegate didTapCellWithContactString:(self.contactType == kContactTypePerson)? self.contact.companyName.lowercaseString
                                                                            :self.contact.parentCompanyName.lowercaseString];
            break;
        default:
            break;
    }
}

#pragma mark - private methods

-(void)rebuilSectionLookUpTable
{
    NSUInteger  actualSecNumber = 0;
    for( NSUInteger secNumber = 0; secNumber < NUM_SECTIONS; secNumber++ )
    {
        if (!self.contact.phones.count && (secNumber == kPhoneSection))
        {
            continue;
        }
        if (!self.contact.addresses.count && (secNumber == kAddressSection))
        {
            continue;
        }
        if (!self.contact.managers.count && (secNumber == kManagersSection))
        {
            continue;
        }
        if(secNumber == kCompanySection &&
           (((self.contactType == kContactTypePerson) && (!self.contact.companyName.length)) ||
            ((self.contactType == kContactTypeOrganization) && (!self.contact.parentCompanyName.length))))
        {
            continue;
        }
        contactDetailSectionLookUpTable[actualSecNumber] = secNumber;
        actualSecNumber++;
    }
    contactDetailSectionLookUpTableSize = actualSecNumber;
}

@end
