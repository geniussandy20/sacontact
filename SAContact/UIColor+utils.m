//
//  UIColor+utils.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "UIColor+utils.h"

@implementation UIColor (utils)

+ (instancetype)uiColorFromRGB:(NSUInteger)rgbValue
{
    return [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0];
}

+ (UIColor *)cellPrimaryTextLabelColor
{
    return [UIColor uiColorFromRGB:0x999999];
}

+(UIColor *)appDisabledActionColor
{
    return [UIColor uiColorFromRGB:0xCCCCCC];
}

+(UIColor*)appTintColor
{
   return [UIColor uiColorFromRGB:0x9d53dc];
}

+(UIColor*)navigationBackgrounColor
{
    return [UIColor uiColorFromRGB:0xF0F0F0];
}

+ (UIColor*)tableSectionHeaderTitleColor
{
    return [UIColor uiColorFromRGB:0x333333];
}

+(UIColor*)tableViewBackgroundColor
{
    return [UIColor uiColorFromRGB:0xeeeeee];
}

@end
