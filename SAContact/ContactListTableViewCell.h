//
//  ContactListTableViewCell.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactModel.h"

@interface ContactListTableViewCell : UITableViewCell

-(void)configureWithContact:(ContactModel*)contact;
-(void)configureWithContact:(ContactModel*)contact searchString:(NSString*)searchString;

@end
