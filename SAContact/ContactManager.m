//
//  ContactManager.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "ContactManager.h"
#import "ContactListModel.h"

#define ContactSucessfullyFetchedStatusCode 200
static NSString *const contactFetchError =  @"com.sacontact.contactFetch";
#define ERROR_INVALID_RESPONSE  1

@interface ContactManager ()

@property (nonatomic,strong) NSURLSessionConfiguration *configuration;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic,weak) id<ContactManagerDelegate> contactManagerDelegate;
@property (nonatomic,strong) NSString *serverUrlString;;
@end

@implementation ContactManager

-(instancetype)initWithDelegate:(id<ContactManagerDelegate>)contactManagerDelegate
{
    self = [super init];
    if (self)
    {
        self.contactManagerDelegate = contactManagerDelegate;
        self.configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:self.configuration];
    }
    return self;
}

-(NSString *)serverUrlString
{
    if (!_serverUrlString)
    {
        _serverUrlString = @"https://api.myjson.com/bins/4tf1w";
    }
    return _serverUrlString;
}

-(void)fetchLatestContacts
{
    if (self.fetchState != ContactManagerFetchStateInProgress)
    {
        self.fetchState = ContactManagerFetchStateInProgress;
        [self.contactManagerDelegate willStartToFetchContacts];
        NSURL *url = [NSURL URLWithString:self.serverUrlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        request.HTTPMethod = @"GET";
        
        
        NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *completionBlockError)
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                              if (completionBlockError)
                                              {
                                                  self.fetchState = ContactManagerFetchStateFailed;
                                                  [self.contactManagerDelegate didFailToFetchedContactsWithError:completionBlockError];
                                              }
                                              else if(httpResponse.statusCode != ContactSucessfullyFetchedStatusCode)
                                              {
                                                  self.fetchState = ContactManagerFetchStateFailed;
                                                  NSError *error = [[NSError alloc] initWithDomain:contactFetchError code:ERROR_INVALID_RESPONSE userInfo:nil];
                                                  [self.contactManagerDelegate didFailToFetchedContactsWithError:error];
                                              }
                                              else
                                              {
                                                  NSError *error;
                                                  ContactListModel* contactList = [[ContactListModel alloc] initWithData:data
                                                                                                                   error:&error];
                                                  if (error)
                                                  {
                                                      self.fetchState = ContactManagerFetchStateFailed;
                                                      [self.contactManagerDelegate didFailToFetchedContactsWithError:error];
                                                  }
                                                  else
                                                  {
                                                      self.fetchState = ContactManagerFetchStateCompleted;
                                                      [self.contactManagerDelegate didSuccesfullyFetchedContacts:contactList.contacts];
                                                  }
                                              }
                                          }];
        [dataTask resume];
    }
    else
    {
        NSLog(@"Fetch already in progress");
    }
}
@end
