//
//  AppFlowController.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppFlowController : NSObject

+ (AppFlowController *) sharedInstance;

-(void)setUpApplicationOnLaunch;


@end
