//
//  ContactDetailTableViewController.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/5/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContactDetailTableViewDelegate <NSObject>
-(void)didTapCellWithContactString:(NSString*)contactString;
@end

@class ContactModel;
@interface ContactDetailTableViewController : UITableViewController

-(instancetype)initWithContact:(ContactModel*)contact contactDetailTableViewDelegate:(id<ContactDetailTableViewDelegate>)delegate;
@end
