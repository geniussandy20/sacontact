//
//  UIColor+utils.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (utils)

+ (instancetype)uiColorFromRGB:(NSUInteger)rgbValue;

+ (UIColor *)cellPrimaryTextLabelColor;

+(UIColor *)appDisabledActionColor;

+(UIColor*)appTintColor;

+(UIColor*)navigationBackgrounColor;

+ (UIColor*)tableSectionHeaderTitleColor;

+(UIColor*)tableViewBackgroundColor;

@end
