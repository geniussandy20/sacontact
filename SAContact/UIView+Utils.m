
//
//  UIView+Utils.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)

-(UIImage*)imageForView
{
    UIGraphicsBeginImageContextWithOptions(self.frame.size,NO,[[UIScreen mainScreen] scale]);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
