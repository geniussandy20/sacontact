//
//  UILabel+Utils.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Utils)

- (void) setText:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color;
- (void) setText:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment;


@end
