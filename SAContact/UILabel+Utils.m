//
//  UILabel+Utils.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "UILabel+Utils.h"

@implementation UILabel (Utils)

- (void) setText:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color
{
    self.text = text;
    self.font = font;
    self.textColor = color;
    self.adjustsFontSizeToFitWidth = YES;
}

- (void) setText:(NSString *)text font:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment
{
    [self setText:text font:font textColor:color];
    self.textAlignment = textAlignment;
}

@end
