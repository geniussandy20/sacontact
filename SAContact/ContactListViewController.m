//
//  ContactListViewController.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import "ContactListViewController.h"
#import "ContactListTableViewCell.h"
#import "ContactManager.h"
#import "UIColor+Utils.h"
#import "UIFont+fonts.h"
#import "ContactDetailTableViewController.h"
#import "ContactListViewController_Extension.h"

#define kCellRowHeight     64.0f
static NSString * const cellIdentifier = @"ContactListTableViewCell";
@interface ContactListViewController ()<UITableViewDataSource,UITableViewDelegate,ContactManagerDelegate,UISearchResultsUpdating,UISearchBarDelegate,ContactDetailTableViewDelegate>
@end


@implementation ContactListViewController

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.filteredResults = [NSMutableArray array];
        self.contactDictionary = [NSMutableDictionary dictionary];
        //Custom
    }
    return self;
}

-(UISearchController *)searchController
{
    if (!_searchController)
    {
        _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        _searchController.searchResultsUpdater = self;
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.searchBar.searchBarStyle = UISearchBarStyleDefault;
        _searchController.searchBar.scopeButtonTitles = @[@"All", @"Name", @"Organization"];
        _searchController.searchBar.delegate = self;
    }
    return _searchController;
}

-(UITableViewController *)tableViewController
{
    if (!_tableViewController)
    {
        _tableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
        [self addChildViewController:_tableViewController];
        UITableView * tableView = _tableViewController.tableView;
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.backgroundView = [[UIView alloc] init];
        tableView.backgroundView.backgroundColor = [UIColor navigationBackgrounColor];
        tableView.allowsSelection = YES;
        tableView.bounces = YES;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableViewController;
}


-(UIActivityIndicatorView *)activityIndicatorView
{
    if(!_activityIndicatorView)
    {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 80.f, 80.f)];
        _activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        _activityIndicatorView.color = [UIColor purpleColor];
        _activityIndicatorView.center = self.tableViewController.tableView.center;
        _activityIndicatorView.hidden = YES;
        _activityIndicatorView.hidesWhenStopped = YES;
        [self.view addSubview:_activityIndicatorView];
        [self.view bringSubviewToFront:_activityIndicatorView];
        
    }
    return _activityIndicatorView;
}

-(UIRefreshControl *)refreshControl
{
    if (!_refreshControl)
    {
        _refreshControl = [[UIRefreshControl alloc] init];
        _refreshControl.backgroundColor = [UIColor purpleColor];
        _refreshControl.tintColor = [UIColor whiteColor];
        [_refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    }
    return _refreshControl;
    
}
-(ContactManager *)contactManager
{
    if (!_contactManager)
    {
        _contactManager = [[ContactManager alloc] initWithDelegate:self];
    }
    return _contactManager;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithKey:WSFontHelveticaNeueMedium withSize:20.0]}];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    self.navigationController.navigationBar.barTintColor = [UIColor navigationBackgrounColor];
    self.navigationController.navigationBar.translucent = NO;
    UISearchBar.appearance.barTintColor = [UIColor navigationBackgrounColor];
    UISearchBar.appearance.tintColor = [UIColor blackColor];
    [UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar self]]].tintColor = [UIColor navigationBackgrounColor];
    
    [self.tableViewController.tableView addSubview:self.refreshControl];

    self.title = NSLocalizedString(@"Contacts",nil);
    self.definesPresentationContext = YES;
    [self addChildViewController:self.tableViewController];
    [self.view addSubview:self.tableViewController.tableView];
    [self setUpLayoutConstraints];
    self.tableViewController.tableView.tableHeaderView = self.searchController.searchBar;
    [self.tableViewController.tableView registerClass:[ContactListTableViewCell class]
                               forCellReuseIdentifier:cellIdentifier];
    [self.tableViewController.tableView reloadData];
    [self.contactManager fetchLatestContacts];
}

-(void)setUpLayoutConstraints
{
    NSLayoutConstraint *tableLeftConstraint = [NSLayoutConstraint constraintWithItem:self.tableViewController.tableView
                                                                           attribute:NSLayoutAttributeLeft
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.view
                                                                           attribute:NSLayoutAttributeLeft
                                                                          multiplier:1.0
                                                                            constant:0.0];
    
    NSLayoutConstraint *tableTopConstraint = [NSLayoutConstraint constraintWithItem:self.tableViewController.tableView
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.view
                                                                          attribute:NSLayoutAttributeTop
                                                                         multiplier:1.0
                                                                           constant:0];
    
    NSLayoutConstraint *tableWidthConstraint = [NSLayoutConstraint constraintWithItem:self.tableViewController.tableView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:1.0
                                                                             constant:0];
    
    NSLayoutConstraint *tableHeightConstraint = [NSLayoutConstraint constraintWithItem:self.tableViewController.tableView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:1.0
                                                                             constant:0];

    
    [NSLayoutConstraint activateConstraints:@[tableLeftConstraint,tableTopConstraint,tableWidthConstraint,tableHeightConstraint]];
    self.tableViewController.tableView.translatesAutoresizingMaskIntoConstraints = NO;
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (self.contactManager.fetchState == ContactManagerFetchStateCompleted)?1:0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.searchController.active && self.searchController.searchBar.text.length)?self.filteredResults.count:self.contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                                      forIndexPath:indexPath];
    
    if ((self.searchController.active && self.searchController.searchBar.text.length))
    {
        [cell configureWithContact:self.filteredResults[(NSUInteger) indexPath.row] searchString:self.searchController.searchBar.text];
    }
    else
    {
        [cell configureWithContact:self.contacts[(NSUInteger) indexPath.row]];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kCellRowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactModel *contact = ((self.searchController.active && self.searchController.searchBar.text.length)?self.filteredResults:self.contacts)[(NSUInteger) indexPath.row];
    ContactDetailTableViewController *contactDetailVC = [[ContactDetailTableViewController alloc] initWithContact:self.contactDictionary[contact.contactObjectIdString] contactDetailTableViewDelegate:self];
    [self.navigationController pushViewController:contactDetailVC animated:YES];
}

#pragma mark - ContactManagerDelegate delegate

-(void)willStartToFetchContacts
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [weakSelf reloadCurrentView];
    });
    
}
-(void)didSuccesfullyFetchedContacts:(NSArray<ContactModel *> *)contacts
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^
    {
        weakSelf.contactDictionary = [weakSelf parseContactListToDictionary:contacts];
        weakSelf.contacts = [weakSelf.contactDictionary allValues];
        if(weakSelf.refreshControl.isRefreshing)
        {
            [weakSelf.refreshControl endRefreshing];
        }
        [weakSelf reloadCurrentView];
        
    });
}

-(void)didFailToFetchedContactsWithError:(NSError *)error
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [weakSelf.contactDictionary removeAllObjects];
        weakSelf.contacts = nil;
        if(weakSelf.refreshControl.isRefreshing)
        {
            [weakSelf.refreshControl endRefreshing];
        }
        [weakSelf reloadCurrentView];
    });
}

#pragma mark - ContactDetailTableViewDelegate delegate

-(void)didTapCellWithContactString:(NSString *)contactString
{
    ContactDetailTableViewController *contactDetailVC = [[ContactDetailTableViewController alloc] initWithContact:self.contactDictionary[contactString.lowercaseString]
                                                                                   contactDetailTableViewDelegate:self];
    [self.navigationController pushViewController:contactDetailVC animated:YES];
}

#pragma mark - UISearchResultsUpdating delegate
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentForSearchText:self.searchController.searchBar.text scope:self.searchController.searchBar.selectedScopeButtonIndex];
}

#pragma mark - UISearchBarDelegate delegate
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.tableViewController.tableView addSubview:self.refreshControl];
}

#pragma mark - private methods
-(NSMutableDictionary*)parseContactListToDictionary:(NSArray<ContactModel*>*)contacts
{
    NSMutableDictionary *contactDictionary = [NSMutableDictionary dictionary];
    for (ContactModel *contact in contacts)
    {
        if(contact.name.length > 0) //Contact is of type person
        {
            if (contactDictionary[contact.contactObjectIdString])
            {
                ContactModel *existingContact = contactDictionary[contact.contactObjectIdString];
                contact.name = existingContact.name.length?existingContact.name:contact.name;
                contact.companyName = existingContact.companyName.length?existingContact.companyName:contact.companyName;
            }
            [contactDictionary setValue:contact forKey:contact.contactObjectIdString];
            
            for (NSString *manager in contact.managers)
            {
                if (!contactDictionary[manager.lowercaseString])
                {
                    ContactModel *managerContact = [[ContactModel alloc] init];
                    managerContact.name = manager;
                    [contactDictionary setValue:managerContact forKey:manager.lowercaseString];
                }
            }
            
            if (!contactDictionary[contact.companyName.lowercaseString])
            {
                ContactModel *organizationContact = [[ContactModel alloc] init];
                organizationContact.companyName = contact.companyName;
                [contactDictionary setValue:organizationContact forKey:contact.companyName.lowercaseString];
            }
        }
        else if(contact.companyName.length > 0) //Contact is of type organization
        {
            if (contactDictionary[contact.contactObjectIdString])
            {
                ContactModel *existingContact = contactDictionary[contact.contactObjectIdString];
                contact.name = existingContact.name.length?existingContact.name:contact.name;
                contact.companyName = existingContact.companyName.length?existingContact.companyName:contact.companyName;
            }
            [contactDictionary setValue:contact forKey:contact.contactObjectIdString];
            
            for (NSString *manager in contact.managers)
            {
                if (!contactDictionary[manager.lowercaseString])
                {
                    ContactModel *managerContact = [[ContactModel alloc] init];
                    managerContact.name = manager;
                    managerContact.companyName = contact.companyName;
                    [contactDictionary setValue:managerContact forKey:manager.lowercaseString];
                }
            }
        }
    }
    return contactDictionary;
}

-(void)handleRefresh:(id)sender
{
    if(!self.searchController.active)
    {
        self.contacts = nil;
        [self reloadCurrentView];
        [self.contactManager fetchLatestContacts];
    }
}

-(void)filterContentForSearchText:(NSString*)searchText scope:(NSInteger)selectedScope
{
    [self.filteredResults removeAllObjects];
    self.filteredResults = [self.contacts mutableCopy];
    
    if (selectedScope == 0)
    {
        [self.filteredResults filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            return ([evaluatedObject matchesAnyFieldsWithString:searchText]);
        }]];
    }
    else if (selectedScope == 1)
    {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS [cd]%@",searchText];
        [self.filteredResults filterUsingPredicate:bPredicate];
    }
    else
    {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"companyName CONTAINS [cd]%@",searchText];
        [self.filteredResults filterUsingPredicate:bPredicate];
    }
    [self reloadCurrentView];
}


-(void)reloadCurrentView
{
    if (self.contactManager.fetchState == ContactManagerFetchStateInProgress)
    {
        [self.activityIndicatorView startAnimating];
        self.activityIndicatorView.hidden = NO;
    }
    else
    {
        [self.activityIndicatorView stopAnimating];
    }
    [[self.tableViewController tableView] reloadData];
}
@end
