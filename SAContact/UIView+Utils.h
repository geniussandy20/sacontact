//
//  UIView+Utils.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)

-(UIImage*)imageForView;

@end

