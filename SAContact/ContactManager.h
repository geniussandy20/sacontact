//
//  ContactManager.h
//  SAContact
//
//  Created by Sandeep Agarwal on 7/4/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactModel.h"

@protocol ContactManagerDelegate
-(void)willStartToFetchContacts;
-(void)didSuccesfullyFetchedContacts:(NSArray<ContactModel*>*)contacts;
-(void)didFailToFetchedContactsWithError:(NSError*)error;
@end

typedef NS_ENUM(NSUInteger , ContactManagerFetchState)
{
    ContactManagerFetchStateNotStarted = 0,
    ContactManagerFetchStateInProgress,
    ContactManagerFetchStateCompleted,
    ContactManagerFetchStateFailed,
};
@interface ContactManager : NSObject

-(instancetype)initWithDelegate:(id<ContactManagerDelegate>)contactManagerDelegate;

-(void)fetchLatestContacts;

@property (nonatomic,assign) ContactManagerFetchState fetchState;
@end
