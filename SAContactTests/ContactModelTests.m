//
//  ContactModelTests.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/6/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ContactModel.h"

@interface ContactModelTests : XCTestCase

@end

@implementation ContactModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testValidateFailureCase
{
    NSString *jsonString = @"{\"contacts\":[{\"parent\":\"ShareFile\",\"managers\":[\"Donald Duck\",\"Mickey Mouse\"]},{\"companyName\":\"Citrix\",\"phones\":[\"(919) 123-4567\",\"(919) 234-1234\"]},{\"companyName\":\"ShareFile\",\"parent\":\"Citrix\",\"addresses\":[\"701 Corporate Center Drive, Raleigh, NC 27607\",\"4140 Parklake Ave #320, Raleigh, NC 27612\"]},{\"name\":\"Donald Duck\",\"phones\":[\"(919) 133-4444\"],\"addresses\":[\"123 Cartoon Road, Miami, FL\"]}]}";
    
    ContactModel *contact = [[ContactModel alloc] initWithString:jsonString error:nil];
    BOOL validate = [contact validate:nil];
    XCTAssertFalse(validate,@"Missing name or companyname in one of the contacts");
}



@end
