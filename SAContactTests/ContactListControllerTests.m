//
//  ContactListControllerTests.m
//  SAContact
//
//  Created by Sandeep Agarwal on 7/6/16.
//  Copyright © 2016 SA. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ContactModel.h"
#import "ContactListModel.h"
#import "ContactListViewController_Extension.h"

@interface ContactListControllerTests : XCTestCase

@end

@implementation ContactListControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testParseContactListToDictionary
{
    ContactModel* contactModel = [[ContactModel alloc] init];
    contactModel.name = @"User1";
    contactModel.companyName = @"Company1";
    contactModel.managers = @[@"Manager1"];
    
    
    ContactListViewController *contactListViewController = [[ContactListViewController alloc] init];
    NSDictionary *dictionary = [contactListViewController parseContactListToDictionary:@[contactModel]];
    XCTAssertTrue(dictionary.count == 3,@"Expected to create 3 objects");
    
    NSString *jsonString = @"{\"contacts\":[{\"companyName\":\"Fun, LLC\",\"parent\":\"ShareFile\",\"managers\":[\"Donald Duck\",\"Mickey Mouse\"]},{\"companyName\":\"Citrix\",\"phones\":[\"(919) 123-4567\",\"(919) 234-1234\"]},{\"companyName\":\"ShareFile\",\"parent\":\"Citrix\",\"addresses\":[\"701 Corporate Center Drive, Raleigh, NC 27607\",\"4140 Parklake Ave #320, Raleigh, NC 27612\"]},{\"name\":\"Donald Duck\",\"phones\":[\"(919) 133-4444\"],\"addresses\":[\"123 Cartoon Road, Miami, FL\"]}]}";
    
    ContactListModel *contactList = [[ContactListModel alloc] initWithString:jsonString error:nil];
    dictionary = [contactListViewController parseContactListToDictionary:contactList.contacts];
    XCTAssertTrue(dictionary.count == 5,@"Expected to create 5 objects");
}


@end
